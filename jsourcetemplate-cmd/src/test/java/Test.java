import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.StandardToStringStyle;

class Test implements java.io.Serializable {

    int test;

    List<String> strings;

    Set<Long> freakingNumbers = new HashSet<Long>();

    public Test addString(String string) {
        this.strings.add(string);
        return this;
    }

    public Test withTest(int test) {
        this.test = test;
        return this;
    }

    public Test addFreakingNumber(Long freakingNumber) {
        this.freakingNumbers.add(freakingNumber);
        return this;
    }

    private static final StandardToStringStyle TO_STRING_STYLE = prepareCommonsToStringStyle();

    @Override
    public String toString() {
        return new ToStringBuilder(this, TO_STRING_STYLE).append("strings", strings).append("freakingNumbers", freakingNumbers).append("test", test).toString();
    }

    private static final StandardToStringStyle prepareCommonsToStringStyle() {
        final StandardToStringStyle toStringStyle = new StandardToStringStyle();
        toStringStyle.setFieldSeparator(", ");
        toStringStyle.setUseShortClassName(true);
        toStringStyle.setUseIdentityHashCode(false);
        toStringStyle.setContentStart("{");
        toStringStyle.setContentEnd("}");
        return toStringStyle;
    }

    private static final long serialVersionUID = 1L;
}
