package com.olinasc.jsourcetemplate.cmd.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public abstract class FileOperation {

	private FileOperation() {
	}

	public static final byte[] readBytes(File file) {

		return read(file).getBytes();
	}

	public static final String read(File file) {

		BufferedReader reader = null;

		try {
			reader = new BufferedReader(new FileReader(file));
			final StringBuilder sb = new StringBuilder();

			String line;

			while ((line = reader.readLine()) != null)
				sb.append(line).append('\n');

			return sb.toString();

		} catch (FileNotFoundException e) {

			throw new IllegalArgumentException("File was not found. File: " + file.getName());

		} catch (IOException e) {

			throw new RuntimeException("Exception while reading file. File: " + file.getName());

		} finally {

			if (reader != null) {

				try {
					reader.close();

				} catch (IOException e) {

					throw new RuntimeException("Exception while closing reader. File: " + file.getName());
				}
			}
		}
	}

	public static final void write(File file, String contents) {

		BufferedWriter writer = null;

		try {
			writer = new BufferedWriter(new FileWriter(file));

			writer.write(contents);
			writer.flush();

		} catch (IOException e) {

			throw new RuntimeException("Exception while writing file. File: " + file.getName());

		} finally {

			if (writer != null) {

				try {
					writer.close();

				} catch (IOException e) {

					throw new RuntimeException("Exception while closing writer. File: " + file.getName());
				}
			}
		}
	}
}