package com.olinasc.jsourcetemplate.cmd;

import java.io.File;
import java.io.FilenameFilter;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.vidageek.mirror.dsl.Mirror;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.olinasc.jsourcetemplate.cmd.util.FileOperation;
import com.olinasc.jsourcetemplate.source.model.JavaSource;
import com.olinasc.jsourcetemplate.source.model.Processor;

public abstract class CmdParameters {

	@Parameter(names = { "-h", "--h", "-help", "--help" }, help = true, hidden = true)
	protected boolean help;

	@Parameter(names = { "-s", "--sourceDir" }, description = "The source directory", required = true, validateWith = ValidateSourceDir.class)
	protected String baseSrcDir;

	@Parameter(names = { "-p", "--packages" }, description = "The packages where java sources will be searched", required = true, variableArity = true)
	protected List<String> packageName;

	@Parameter(names = { "-cm", "--chain-methods" }, description = "Generates chain methods.")
	@ProcessorOption(Processor.CHAIN_METHOD)
	private boolean chainMethods;

	@Parameter(names = { "-ser", "--serializable" }, description = "Implements Serializable")
	@ProcessorOption(Processor.SERIALIZABLE)
	private boolean serializable;

	@Parameter(names = { "-cts", "--commons-to-string" }, description = "Generates a toString method using all field but the static ones.")
	@ProcessorOption(Processor.COMMONS_TO_STRING)
	private boolean commonsToString;

	@Parameter(names = { "--persist" }, description = "Persists changes")
	protected boolean persist;

	@Parameter(names = { "--print" }, description = "Prints the processing result")
	protected boolean printResult;

	public Set<Processor> parseOptions() {

		final List<Field> fields = new Mirror().on(CmdParameters.class)
				.reflectAll().fields();
		final Set<Processor> options = new HashSet<Processor>();

		for (Field f : fields) {

			f.setAccessible(true);

			try {

				if (f.isAnnotationPresent(ProcessorOption.class)
						&& f.getBoolean(this)) {

					options.add(f.getAnnotation(ProcessorOption.class).value());
				}

			} catch (IllegalAccessException e) {

				throw new RuntimeException(
						"Error while catching processor options", e);
			}
		}

		return options;
	}

	protected Map<JavaSource, File> buildSetOfSourceFiles() {

		final Map<JavaSource, File> sourceMap = new HashMap<JavaSource, File>();

		for (String packagePath : packageName) {

			final File path = new File(baseSrcDir
					+ packagePath.replace('.', File.separatorChar));

			if (!path.exists() || !path.isDirectory())
				throw new ParameterException(
						"Wrong usage: source directory must be a valid directory: "
								+ path.getName());

			final File[] javaFiles = path.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					return name.endsWith(".java");
				}
			});

			for (File filePath : javaFiles)
				sourceMap.put(new JavaSource(FileOperation.read(filePath)),
						filePath);
		}

		return sourceMap;
	}

	public boolean isSerializable() {
		return serializable;
	}

	public void setSerializable(boolean serializable) {
		this.serializable = serializable;
	}

	public boolean isChainMethods() {
		return chainMethods;
	}

	public void setChainMethods(boolean chainMethods) {
		this.chainMethods = chainMethods;
	}

	public boolean isCommonsToString() {
		return commonsToString;
	}

	public void setCommonsToString(boolean commonsToString) {
		this.commonsToString = commonsToString;
	}

	public static class ValidateSourceDir implements IParameterValidator {
		@Override
		public void validate(String name, String value)
				throws ParameterException {

			final File f = new File(value);

			if (!f.exists() || !f.isDirectory())
				throw new ParameterException(
						"Wrong usage: source directory must be a valid directory: "
								+ f.getName());
		}
	}

	@Target({ ElementType.FIELD })
	@Retention(RetentionPolicy.RUNTIME)
	private static @interface ProcessorOption {
		Processor value();
	}
}
