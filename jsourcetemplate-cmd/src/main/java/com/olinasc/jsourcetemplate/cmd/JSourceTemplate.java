package com.olinasc.jsourcetemplate.cmd;

import java.io.File;
import java.util.Map;
import java.util.Set;

import com.beust.jcommander.JCommander;
import com.olinasc.jsourcetemplate.cmd.util.FileOperation;
import com.olinasc.jsourcetemplate.processor.api.SourceProcessor;
import com.olinasc.jsourcetemplate.source.model.JavaSource;
import com.olinasc.jsourcetemplate.source.model.Processor;

public final class JSourceTemplate extends CmdParameters {

	public static void main(String[] args) {

		args = new String[] {
				"-cm",
				"-cts",
				"-ser",
				"--persist",
				"--print",
				"-s",
				"src/test/java",
				"-p",
				"" };

		final JSourceTemplate m = new JSourceTemplate();

		final JCommander jc = new JCommander(m, args);
		jc.setProgramName("Main");

		if (m.help) {
			jc.usage();
			return;
		}

		final Map<JavaSource, File> sources = m.buildSetOfSourceFiles();
		final Set<Processor> parseOptions = m.parseOptions();

		SourceProcessor.processSourceFiles(sources.keySet(), parseOptions);

		if (m.printResult)
			for (JavaSource source : sources.keySet())
				System.out.println(source.getContentsAsString());

		if (m.persist)
			for (JavaSource source : sources.keySet())
				FileOperation.write(sources.get(source), source.getContentsAsString());
	}
}