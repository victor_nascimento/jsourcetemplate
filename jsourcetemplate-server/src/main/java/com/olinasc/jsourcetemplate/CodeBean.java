package com.olinasc.jsourcetemplate;

import japa.parser.ParseException;

import java.io.Serializable;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.olinasc.jsourcetemplate.cmd.CmdParameters;
import com.olinasc.jsourcetemplate.cmd.JSourceTemplate;
import com.olinasc.jsourcetemplate.processor.api.SourceProcessor;
import com.olinasc.jsourcetemplate.source.model.JavaSource;
import com.olinasc.jsourcetemplate.source.model.Processor;

@ManagedBean
@ViewScoped
public class CodeBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private CmdParameters params = new JSourceTemplate();

	private String oldCode = "";
	private String newCode = "";

	public void enhanceCode() {

		final Set<Processor> parseOptions = params.parseOptions();

		JavaSource source = new JavaSource(oldCode);

		try {
			SourceProcessor.processSourceFile(source, parseOptions);

		} catch (RuntimeException e) {

			FacesContext ctx = FacesContext.getCurrentInstance();

			if (e.getCause() instanceof ParseException) {

				ctx.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_WARN,
						"The code to enchance is not proper", null));

			} else {

				ctx.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR, "Server Internal Error",
						null));
			}

			oldCode = "";
			newCode = "";

			e.printStackTrace();
			return;
		}

		newCode = source.getContentsAsString().trim();

	}

	public CmdParameters getParams() {
		return params;
	}

	public void setParams(CmdParameters params) {
		this.params = params;
	}

	public String getOldCode() {
		return oldCode;
	}

	public void setOldCode(String oldCode) {
		this.oldCode = oldCode;
	}

	public String getNewCode() {
		return newCode;
	}

	public void setNewCode(String newCode) {
		this.newCode = newCode;
	}

}
