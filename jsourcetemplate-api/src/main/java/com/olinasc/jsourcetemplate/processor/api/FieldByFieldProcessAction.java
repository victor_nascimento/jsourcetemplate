package com.olinasc.jsourcetemplate.processor.api;

import japa.parser.ast.CompilationUnit;
import japa.parser.ast.body.FieldDeclaration;

import java.util.HashSet;
import java.util.Set;

import com.olinasc.jsourcetemplate.util.CompilationUnitUtils;

/**
 * A utility class to implement processors that take action for each field
 * declaration in a class.
 * 
 * @author victorolinasc
 */
public abstract class FieldByFieldProcessAction implements ProcessAction {

	public abstract Set<FieldDeclaration> filterFields(CompilationUnit unit);

	public abstract String processField(FieldDeclaration field, CompilationUnit unit);

	@Override
	public boolean shouldProcess(CompilationUnit unit) {
		return CompilationUnitUtils.isClass(unit.getTypes().get(0));
	}

	@Override
	public void processSource(CompilationUnit unit) {

		final Set<FieldDeclaration> fields = filterFields(unit);

		if (fields.size() == 0)
			return;

		final Set<String> templates = new HashSet<String>();

		for (FieldDeclaration fieldDeclaration : fields) {

			final String template = processField(fieldDeclaration, unit);

			if (template != null)
				templates.add(template);
		}

		if (templates.size() > 0)
			includeTemplates(templates, unit);
	}

	private void includeTemplates(Iterable<String> templates, CompilationUnit unit) {

		final StringBuilder sb = new StringBuilder();

		for (String string : templates)
			sb.append(string).append("\n");

		CompilationUnitUtils.includeMethods(sb.toString(), unit);
	}
}
