package com.olinasc.jsourcetemplate.processor.api;

import japa.parser.JavaParser;
import japa.parser.ParseException;
import japa.parser.ast.CompilationUnit;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import com.olinasc.jsourcetemplate.source.model.JavaSource;
import com.olinasc.jsourcetemplate.source.model.Processor;

public abstract class SourceProcessor {

	private SourceProcessor() {
	}

	public static final void processSourceFile(JavaSource source,
			Iterable<Processor> processOptions) {

		final InputStream is = new ByteArrayInputStream(source.getContents());

		CompilationUnit compilationUnit = null;

		try {
			compilationUnit = JavaParser.parse(is);

		} catch (ParseException e) {

			throw new RuntimeException("Error while parsing input stream. Stream: \n" + new String(source.getContents()), e);
		}

		for (Processor processor : processOptions) {

			if (!processor.processor().shouldProcess(compilationUnit))
				continue;

			processor.processor().processSource(compilationUnit);
		}

		if (!source.getContentsAsString().equals(compilationUnit.toString()))
			source.setContents(compilationUnit.toString().getBytes());
	}

	public static final void processSourceFiles(Iterable<JavaSource> sources,
			Iterable<Processor> processOptions) {

		for (JavaSource sourceFile : sources)
			processSourceFile(sourceFile, processOptions);
	}
}
