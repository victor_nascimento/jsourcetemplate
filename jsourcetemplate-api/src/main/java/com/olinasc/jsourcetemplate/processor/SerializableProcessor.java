package com.olinasc.jsourcetemplate.processor;

import japa.parser.ASTHelper;
import japa.parser.ast.CompilationUnit;
import japa.parser.ast.body.ClassOrInterfaceDeclaration;
import japa.parser.ast.body.FieldDeclaration;
import japa.parser.ast.body.VariableDeclarator;
import japa.parser.ast.body.VariableDeclaratorId;
import japa.parser.ast.expr.LongLiteralExpr;
import japa.parser.ast.type.ClassOrInterfaceType;
import japa.parser.ast.type.PrimitiveType;
import japa.parser.ast.type.PrimitiveType.Primitive;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import com.olinasc.jsourcetemplate.processor.api.ProcessAction;
import com.olinasc.jsourcetemplate.util.CompilationUnitUtils;

public class SerializableProcessor implements ProcessAction {

	private static final FieldDeclaration SERIALIZABLE_FIELD = createSerializableField();
	private static final ClassOrInterfaceType SERIALIZABLE_TYPE = new ClassOrInterfaceType("java.io.Serializable");

	private static final String SERIALIZABLE_FIELD_NAME = "serialVersionUID";

	@Override
	public boolean shouldProcess(CompilationUnit unit) {
		return CompilationUnitUtils.isClass(unit.getTypes().get(0));
	}

	@Override
	public void processSource(CompilationUnit unit) {

		final ClassOrInterfaceDeclaration clazz = (ClassOrInterfaceDeclaration) unit.getTypes().get(0);

		final List<ClassOrInterfaceType> interfaces = clazz.getImplements() != null ? clazz.getImplements()
				: new ArrayList<ClassOrInterfaceType>();

		for (ClassOrInterfaceType type : interfaces)
			if (type.getName().equals("Serializable"))
				return;

		interfaces.add(SERIALIZABLE_TYPE);
		clazz.setImplements(interfaces);

		if (!CompilationUnitUtils.hasField(clazz, SERIALIZABLE_FIELD_NAME))
			ASTHelper.addMember(clazz, SERIALIZABLE_FIELD);
	}

	private static FieldDeclaration createSerializableField() {

		final int modifiers = Modifier.PRIVATE | Modifier.STATIC | Modifier.FINAL;
		final VariableDeclarator variable = new VariableDeclarator(new VariableDeclaratorId(SERIALIZABLE_FIELD_NAME), new LongLiteralExpr(
				"1L"));
		final FieldDeclaration field = ASTHelper.createFieldDeclaration(modifiers, new PrimitiveType(Primitive.Long), variable);

		return field;
	}
}
