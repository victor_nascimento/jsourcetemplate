package com.olinasc.jsourcetemplate.processor.api;

import japa.parser.ast.CompilationUnit;

/**
 * Contract for implementing a processor. All processors must know which
 * compilation units to process and how to process them.
 * 
 * @author victorolinasc
 */
public interface ProcessAction {

	/**
	 * Filters units to avoid computation.
	 * 
	 * @param unit
	 * @return True if this unit will be processed by this processor, false
	 *         otherwise.
	 */
	boolean shouldProcess(CompilationUnit unit);

	/**
	 * Enchance the compilation unit
	 * 
	 * @param unit
	 *            The Compilation Unit that represents a source code.
	 */
	void processSource(CompilationUnit unit);

}
