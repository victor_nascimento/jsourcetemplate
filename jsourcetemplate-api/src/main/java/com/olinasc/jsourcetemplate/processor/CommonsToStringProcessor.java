package com.olinasc.jsourcetemplate.processor;

import japa.parser.ASTHelper;
import japa.parser.ast.CompilationUnit;
import japa.parser.ast.body.BodyDeclaration;
import japa.parser.ast.body.FieldDeclaration;
import japa.parser.ast.body.ModifierSet;
import japa.parser.ast.body.TypeDeclaration;
import japa.parser.ast.body.VariableDeclarator;
import japa.parser.ast.body.VariableDeclaratorId;
import japa.parser.ast.expr.MethodCallExpr;
import japa.parser.ast.type.ClassOrInterfaceType;
import japa.parser.ast.type.Type;

import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.olinasc.jsourcetemplate.processor.api.ProcessAction;
import com.olinasc.jsourcetemplate.util.CompilationUnitUtils;

public final class CommonsToStringProcessor implements ProcessAction {

	private static final String TO_STRING_STYLE = prepareToStringStyle();
	private static final String TO_STRING = prepareToStringMask();

	private static final FieldDeclaration TO_STRING_FIELD = createToStringField();

	private static final String[] IMPORTS = { "org.apache.commons.lang3.builder.ToStringBuilder",
			"org.apache.commons.lang3.builder.StandardToStringStyle" };

	@Override
	public boolean shouldProcess(CompilationUnit unit) {

		if (CompilationUnitUtils.isClass(unit.getTypes().get(0)))
			return !CompilationUnitUtils.hasMethod(unit, "toString", "String", null);

		return false;
	}

	@Override
	public void processSource(CompilationUnit unit) {

		final Set<FieldDeclaration> fields = filterFields(unit);

		if (fields.size() == 0)
			return;

		// imports
		CompilationUnitUtils.addImports(unit, IMPORTS);
		// To string style field
		ASTHelper.addMember(unit.getTypes().get(0), TO_STRING_FIELD);

		final StringBuilder sb = new StringBuilder();

		for (FieldDeclaration fieldDeclaration : fields) {
			final String fieldName = fieldDeclaration.getVariables().get(0).getId().getName();
			sb.append(".append(\"").append(fieldName).append("\", ").append(fieldName).append(")");
		}

		final String methods = TO_STRING.replace("${appendMask}", sb.toString()) + TO_STRING_STYLE;

		CompilationUnitUtils.includeMethods(methods, unit);

	}

	private Set<FieldDeclaration> filterFields(CompilationUnit unit) {

		final Set<FieldDeclaration> fields = new HashSet<FieldDeclaration>();
		final List<TypeDeclaration> types = unit.getTypes();

		for (TypeDeclaration typeDeclaration : types) {

			final List<BodyDeclaration> members = typeDeclaration.getMembers();

			for (BodyDeclaration bodyDeclaration : members) {

				if (bodyDeclaration instanceof FieldDeclaration) {

					int modifiers = ((FieldDeclaration) bodyDeclaration).getModifiers();

					if (ModifierSet.hasModifier(modifiers, Modifier.STATIC))
						continue;

					fields.add((FieldDeclaration) bodyDeclaration);
				}
			}
		}

		return fields;
	}

	private static final String prepareToStringStyle() {
		return " private static final StandardToStringStyle prepareCommonsToStringStyle() {" +
				"final StandardToStringStyle toStringStyle = new StandardToStringStyle();" +
				"toStringStyle.setFieldSeparator(\", \");" +
				"toStringStyle.setUseShortClassName(true);" +
				"toStringStyle.setUseIdentityHashCode(false);" +
				"toStringStyle.setContentStart(\"{\");" +
				"toStringStyle.setContentEnd(\"}\");" +
				"return toStringStyle;}";
	}

	private static final String prepareToStringMask() {
		return " @Override public String toString() {" +
				"return new ToStringBuilder(this, TO_STRING_STYLE)" +
				"${appendMask}.toString(); }";
	}

	private static FieldDeclaration createToStringField() {

		final int modifiers = Modifier.PRIVATE | Modifier.STATIC | Modifier.FINAL;
		final Type type = new ClassOrInterfaceType("StandardToStringStyle");

		final VariableDeclarator variable = new VariableDeclarator(new VariableDeclaratorId("TO_STRING_STYLE"));
		variable.setInit(new MethodCallExpr(null, "prepareCommonsToStringStyle"));

		return ASTHelper.createFieldDeclaration(modifiers, type, variable);
	}
}