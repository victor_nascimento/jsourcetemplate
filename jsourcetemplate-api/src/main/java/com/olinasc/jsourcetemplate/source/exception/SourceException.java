package com.olinasc.jsourcetemplate.source.exception;

public class SourceException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public SourceException(String message) {
		super(message);
	}
}
