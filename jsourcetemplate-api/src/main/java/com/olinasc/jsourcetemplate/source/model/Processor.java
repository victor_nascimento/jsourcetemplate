package com.olinasc.jsourcetemplate.source.model;

import com.olinasc.jsourcetemplate.processor.ChainMethodProcessor;
import com.olinasc.jsourcetemplate.processor.CommonsToStringProcessor;
import com.olinasc.jsourcetemplate.processor.SerializableProcessor;
import com.olinasc.jsourcetemplate.processor.api.ProcessAction;

public enum Processor {

	CHAIN_METHOD(new ChainMethodProcessor()),

	COMMONS_TO_STRING(new CommonsToStringProcessor()),

	SERIALIZABLE(new SerializableProcessor());

	private ProcessAction processAction;

	private Processor(ProcessAction processAction) {
		this.processAction = processAction;
	}

	public ProcessAction processor() {
		return processAction;
	}
}
