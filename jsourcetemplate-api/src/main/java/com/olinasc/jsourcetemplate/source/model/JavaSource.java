package com.olinasc.jsourcetemplate.source.model;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.olinasc.jsourcetemplate.util.ToStringStyle;

public class JavaSource implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final ToStringStyle STYLE = new ToStringStyle();

	private byte[] contents;

	public JavaSource(byte[] source) {
		this.setContents(source);
	}

	public JavaSource(String source) {
		this.setContents(source.getBytes());
	}

	public InputStream getInputStream() {
		return new ByteArrayInputStream(getContents());
	}

	public byte[] getContents() {
		return contents;
	}

	public String getContentsAsString() {
		return new String(getContents());
	}

	public void setContents(byte[] contents) {
		this.contents = contents;
	}

	public void setContents(String contents) {
		this.contents = contents.getBytes();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, STYLE)
				.append("contents", new String(getContents()))
				.toString();
	}

}