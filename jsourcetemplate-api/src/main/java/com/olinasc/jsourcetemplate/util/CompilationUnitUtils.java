package com.olinasc.jsourcetemplate.util;

import japa.parser.ASTHelper;
import japa.parser.JavaParser;
import japa.parser.ParseException;
import japa.parser.ast.CompilationUnit;
import japa.parser.ast.ImportDeclaration;
import japa.parser.ast.body.BodyDeclaration;
import japa.parser.ast.body.ClassOrInterfaceDeclaration;
import japa.parser.ast.body.FieldDeclaration;
import japa.parser.ast.body.MethodDeclaration;
import japa.parser.ast.body.Parameter;
import japa.parser.ast.body.TypeDeclaration;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import com.olinasc.jsourcetemplate.source.exception.SourceException;

/**
 * This is a set of utility methods that ease manipulating a CompilationUnit.
 * 
 * @author victorolinasc
 * 
 */
public abstract class CompilationUnitUtils {

	private static final String CLASS_TEMPLATE = "class Clazz { ${methods} }";

	private CompilationUnitUtils() {
	}

	/**
	 * Finds and returns the name of the first type in the CompilationUnit
	 * 
	 * @param compUnit
	 * @return The name of the first type in this unit.
	 */
	public static final String getTypeName(CompilationUnit compUnit) {
		return compUnit.getTypes().get(0).getName();
	}

	/**
	 * Returns all methodDeclarations in this Unit. This will ALSO return
	 * methods in inner classes and so on.
	 * 
	 * @param compUnit
	 * @return List of found methods.
	 */
	public static final List<MethodDeclaration> getMethods(CompilationUnit compUnit) {

		final List<MethodDeclaration> methods = new ArrayList<MethodDeclaration>();

		final List<TypeDeclaration> types = compUnit.getTypes();

		for (TypeDeclaration typeDeclaration : types) {

			for (BodyDeclaration bodyDeclaration : typeDeclaration.getMembers()) {

				if (bodyDeclaration instanceof MethodDeclaration)
					methods.add((MethodDeclaration) bodyDeclaration);
			}
		}

		return methods;
	}

	/**
	 * Determines if this Type declaration is a concrete class and not an Enum,
	 * Interface or Annotation.
	 * 
	 * @param typeDeclaration
	 * @return True if it is a concrete class false otherwise
	 */
	public static final boolean isClass(TypeDeclaration typeDeclaration) {
		return typeDeclaration instanceof ClassOrInterfaceDeclaration && !((ClassOrInterfaceDeclaration) typeDeclaration).isInterface();
	}

	/**
	 * This method includes all methods that are described in the template
	 * string into the compilation unit.
	 * 
	 * @param template
	 *            Represents the method declarations
	 * @param unit
	 *            Unit to have the methods added to.
	 */
	public static final void includeMethods(String template, CompilationUnit unit) {

		final String clazz = CLASS_TEMPLATE.replace("${methods}", template);

		try {
			final CompilationUnit parsed = JavaParser
					.parse(new ByteArrayInputStream(clazz.getBytes()));
			final TypeDeclaration firstType = unit.getTypes().get(0);
			final List<MethodDeclaration> methods = CompilationUnitUtils
					.getMethods(parsed);

			for (MethodDeclaration methodDeclaration : methods)
				ASTHelper.addMember(firstType, methodDeclaration);

		} catch (ParseException e) {

			throw new SourceException(
					"An error ocurred while processing adding methods to a CompilationUnit. \nTemplate: \n" + template);
		}
	}

	/**
	 * Checks whether a given CompilationUnit has a method with the given name,
	 * returnType and parameters (the method signature). If the paramTypes are
	 * null, the method is assumed to have no parameters.
	 * 
	 * @param unit
	 * @param methodName
	 * @param returnType
	 * @param paramTypes
	 * @return true if the unit has a method with the given signature, false
	 *         otherwise.
	 */
	public static boolean hasMethod(CompilationUnit unit, String methodName, String returnType, List<String> paramTypes) {

		final List<BodyDeclaration> members = unit.getTypes().get(0).getMembers();

		for (BodyDeclaration bodyDeclaration : members) {

			if (!(bodyDeclaration instanceof MethodDeclaration))
				continue;

			if (isMethod((MethodDeclaration) bodyDeclaration, methodName, returnType, paramTypes))
				return true;
		}

		return false;
	}

	/**
	 * Checks whether a given MethodDeclaration has the given name, returnType
	 * and parameters (the method signature). If the paramTypes are null, the
	 * method is assumed to have no parameters.
	 * 
	 * @param unit
	 * @param methodName
	 * @param returnType
	 * @param paramTypes
	 * @return true if the declaration has the given method signature, false
	 *         otherwise.
	 */
	public static boolean isMethod(MethodDeclaration method, String methodName, String returnType, List<String> paramTypes) {

		final boolean isSameName = method.getName().equals(methodName);
		final boolean isSameReturnType = method.getType().toString().equals(returnType);

		if (!isSameName || !isSameReturnType)
			return false;

		final List<Parameter> parameters = method.getParameters();

		int paramsSize = paramTypes != null ? paramTypes.size() : 0;
		int parametersSize = parameters != null ? parameters.size() : 0;

		// Different arity
		if (paramsSize != parametersSize)
			return false;

		// no parameters to test
		if (paramsSize == 0 && parametersSize == 0)
			return true;

		boolean isSame = true;

		for (int i = 0; i < parameters.size(); i++) {

			if (!parameters.get(i).getType().toString().equals(paramTypes.get(i))) {

				isSame = false;
				break;
			}
		}

		if (isSame)
			return true;

		return false;
	}

	/**
	 * Adds the list of imports to the current CompilationUnit.
	 * 
	 * @param unit
	 * @param importNames
	 */
	public static void addImports(CompilationUnit unit, String... importNames) {

		if (importNames == null || importNames.length == 0)
			return;

		final List<ImportDeclaration> imports = unit.getImports() != null ? unit.getImports() : new ArrayList<ImportDeclaration>();

		for (String importName : importNames)
			imports.add(new ImportDeclaration(ASTHelper.createNameExpr(importName), false, false));

		unit.setImports(imports);
	}

	public static boolean hasField(ClassOrInterfaceDeclaration clazz, String fieldName) {

		final List<BodyDeclaration> members = clazz.getMembers();

		for (BodyDeclaration member : members) {

			if (!(member instanceof FieldDeclaration))
				continue;

			if (((FieldDeclaration) member).getVariables().get(0).getId().getName().equals(fieldName))
				return true;
		}

		return false;
	}
}