package com.olinasc.jsourcetemplate.util;

import org.apache.commons.lang3.builder.StandardToStringStyle;

public class ToStringStyle extends StandardToStringStyle {

	private static final long serialVersionUID = 1L;

	public ToStringStyle() {
		super();
		setFieldSeparator(", ");
		setUseShortClassName(true);
		setUseIdentityHashCode(false);
		setContentStart("{");
		setContentEnd("}");
	}
}
