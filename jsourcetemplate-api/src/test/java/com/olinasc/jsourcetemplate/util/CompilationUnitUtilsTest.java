package com.olinasc.jsourcetemplate.util;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import japa.parser.JavaParser;
import japa.parser.ParseException;
import japa.parser.ast.CompilationUnit;

import java.io.ByteArrayInputStream;
import java.util.Arrays;

import org.junit.Test;

public class CompilationUnitUtilsTest {

	@Test
	public void testHasMethod() throws ParseException {

		CompilationUnit unit = JavaParser
				.parse(new ByteArrayInputStream("class Test { public String toString(){ return null;}}".getBytes()));

		boolean hasMethod = CompilationUnitUtils.hasMethod(unit, "toString", "String", null);

		assertThat(hasMethod, is(true));

		hasMethod = CompilationUnitUtils.hasMethod(unit, "toString", "void", null);

		assertThat(hasMethod, is(false));

		hasMethod = CompilationUnitUtils.hasMethod(unit, "toString", "String", Arrays.asList("String"));

		assertThat(hasMethod, is(false));
	}

}
