package com.olinasc.jsourcetemplate.util;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import japa.parser.JavaParser;
import japa.parser.ParseException;
import japa.parser.ast.CompilationUnit;

import java.io.ByteArrayInputStream;
import java.util.Collection;

import com.olinasc.jsourcetemplate.processor.api.SourceProcessor;
import com.olinasc.jsourcetemplate.source.model.JavaSource;
import com.olinasc.jsourcetemplate.source.model.Processor;

public abstract class TestUtils {

	private TestUtils() {
	}

	public static void processClass(String clazz, String expectedClazz, Collection<Processor> processors) {

		assertThat(clazz, is(notNullValue()));
		assertThat(expectedClazz, is(notNullValue()));
		assertThat(processors, is(notNullValue()));
		assertThat(processors.size(), is(greaterThan(0)));

		final JavaSource source = new JavaSource(clazz);

		SourceProcessor.processSourceFile(source, processors);

		CompilationUnit unit = null;

		try {
			unit = JavaParser.parse(new ByteArrayInputStream(expectedClazz.getBytes()));

		} catch (ParseException e) {

			throw new RuntimeException("Could not parse expected class. \n" + expectedClazz, e);
		}

		// System.out.println("Class: \n" + source.getContentsAsString() +
		// "\n");
		// System.out.println("Expected: \n" + unit.toString());

		assertThat(source.getContentsAsString(), is(unit.toString()));
	}
}
