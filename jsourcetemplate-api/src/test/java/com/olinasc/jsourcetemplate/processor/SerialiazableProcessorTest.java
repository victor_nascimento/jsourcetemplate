package com.olinasc.jsourcetemplate.processor;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;

import com.olinasc.jsourcetemplate.source.model.Processor;
import com.olinasc.jsourcetemplate.util.TestUtils;

public class SerialiazableProcessorTest {

	private Collection<Processor> processors = Arrays.asList(Processor.SERIALIZABLE);

	@Test
	public void testClassWithSerializable() {

		String clazz = "class Test implements Serializable {} ";
		TestUtils.processClass(clazz, clazz, processors);
	}

	@Test
	public void testEnum() {

		String clazz = "enum Test {} ";
		TestUtils.processClass(clazz, clazz, processors);
	}

	@Test
	public void testInterface() {

		String clazz = "interface Test {} ";
		TestUtils.processClass(clazz, clazz, processors);
	}

	@Test
	public void testAnnotation() {

		String clazz = "@interface Test {} ";
		TestUtils.processClass(clazz, clazz, processors);
	}

	@Test
	public void testClassWithSerialVersionUIDField() {

		String clazz = "class Test { long serialVersionUID;} ";
		String expectedClazz = "class Test implements java.io.Serializable { long serialVersionUID; }";

		TestUtils.processClass(clazz, expectedClazz, processors);
	}

	@Test
	public void testTargetClass() {

		String clazz = "class Test { } ";
		String expectedClazz = "class Test implements java.io.Serializable { private static final long serialVersionUID = 1L; }";

		TestUtils.processClass(clazz, expectedClazz, processors);
	}

}
