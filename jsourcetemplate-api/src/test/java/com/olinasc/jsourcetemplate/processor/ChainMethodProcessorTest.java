package com.olinasc.jsourcetemplate.processor;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.olinasc.jsourcetemplate.source.model.Processor;
import com.olinasc.jsourcetemplate.util.TestUtils;

public class ChainMethodProcessorTest {

	private List<Processor> processors = Arrays.asList(Processor.CHAIN_METHOD);

	@Test
	public void testParsingClass() {

		String clazz = "class Test { private String test = \"\";}";
		String expectedClazz = "class Test { private String test = \"\"; public Test withTest(String test) { this.test = test; return this;}}";

		TestUtils.processClass(clazz, expectedClazz, processors);
	}

	@Test
	public void testParsingClassWithPrimitive() {

		String clazz = "class Test { private int test;}";
		String expectedClazz = "class Test { private int test; public Test withTest(int test) { this.test = test; return this;}}";

		TestUtils.processClass(clazz, expectedClazz, processors);
	}

	@Test
	public void testParsingClassWithListField() {

		String clazz = "class Test { List<String> tests; }";
		String expectedClazz = "class Test { List<String> tests; public Test addTest(String test) { this.tests.add(test); return this;}}";

		TestUtils.processClass(clazz, expectedClazz, processors);
	}

	@Test
	public void testParsingClassWithSetField() {

		String clazz = "class Test { Set<String> tests; }";
		String expectedClazz = "class Test { Set<String> tests; public Test addTest(String test) { this.tests.add(test); return this;}}";

		TestUtils.processClass(clazz, expectedClazz, processors);
	}

	@Test
	public void testParsingStaticField() {

		String clazz = "class Test { static int test;}";
		TestUtils.processClass(clazz, clazz, processors);
	}

	@Test
	public void testParsingFinalField() {

		String clazz = "class Test{ final int test = 1;}";
		TestUtils.processClass(clazz, clazz, processors);
	}

	@Test
	public void testParsingEnum() {

		String clazz = "enum Test { TEST; private int test = 1;}";
		TestUtils.processClass(clazz, clazz, processors);
	}

	@Test
	public void testParsingAnnotation() {

		String clazz = "public @interface Test { String test();}";
		TestUtils.processClass(clazz, clazz, processors);
	}
}