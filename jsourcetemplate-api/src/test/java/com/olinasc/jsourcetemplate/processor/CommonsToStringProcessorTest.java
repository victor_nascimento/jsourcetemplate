package com.olinasc.jsourcetemplate.processor;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.olinasc.jsourcetemplate.source.model.Processor;
import com.olinasc.jsourcetemplate.util.TestUtils;

public class CommonsToStringProcessorTest {

	private List<Processor> processors = Arrays.asList(Processor.COMMONS_TO_STRING);

	@Test
	public void testClassWithExistingToString() {

		String clazz = "class Test { public String toString(){ return \"\";}} ";
		TestUtils.processClass(clazz, clazz, processors);

		clazz = "class Test { @override public String toString(){ return \"\";}} ";
		TestUtils.processClass(clazz, clazz, processors);

		clazz = "class Test { public String toString() { return null;}} ";
		TestUtils.processClass(clazz, clazz, processors);
	}

	@Test
	public void testClassWithOnlyStaticFields() {

		String clazz = "class Test { static int i;} ";
		TestUtils.processClass(clazz, clazz, processors);

		clazz = "class Test { static int a, b, c;} ";
		TestUtils.processClass(clazz, clazz, processors);

		clazz = "class Test { static int a, b, c; static String d, e, f;} ";
		TestUtils.processClass(clazz, clazz, processors);
	}

	@Test
	public void testClassWithDefaultNonStaticField() {

		String clazz = "class Test { int test;}";
		String expectedClazz = createExpectedClass("Test", "int test;", ".append(\"test\", test)");

		TestUtils.processClass(clazz, expectedClazz, processors);
	}

	private String createExpectedClass(String className, String fields, String append) {

		return String.format(
				"import org.apache.commons.lang3.builder.ToStringBuilder;  import org.apache.commons.lang3.builder.StandardToStringStyle; "
						+ "class %s { %s "
						+ "private static final StandardToStringStyle TO_STRING_STYLE = prepareCommonsToStringStyle();"
						+ "@Override public String toString() { return new ToStringBuilder(this, TO_STRING_STYLE)"
						+ "%s.toString();} "
						+ "private static final StandardToStringStyle prepareCommonsToStringStyle() {"
						+ "final StandardToStringStyle toStringStyle = new StandardToStringStyle();"
						+ "toStringStyle.setFieldSeparator(\", \");"
						+ "toStringStyle.setUseShortClassName(true);"
						+ "toStringStyle.setUseIdentityHashCode(false);"
						+ "toStringStyle.setContentStart(\"{\");"
						+ "toStringStyle.setContentEnd(\"}\");"
						+ "return toStringStyle;} }", className, fields, append);
	}
}